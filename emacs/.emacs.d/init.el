;;------------------
;;---- Packages ----
;;------------------
;; Put customize variables in separate file
(setq custom-file "~/.emacs.d/customize.el")
(load custom-file)

(require 'package) 
(add-to-list 'package-archives
	     '("melpa" . "http://melpa.org/packages/") t)
(package-initialize) 
(setq package-check-signature nil)
(package-install-selected-packages)

;; Flycheck Settings
(add-to-list 'display-buffer-alist
             `(,(rx bos "*Flycheck errors*" eos)
              (display-buffer-reuse-window
               display-buffer-in-side-window)
              (side            . bottom)
              (reusable-frames . visible)
              (window-height   . 0.2)))
(require 'flycheck)
(require 'flymd)
(global-flycheck-mode 1)

;; Company Mode Settings
(require 'company)
(add-hook 'after-init-hook 'global-company-mode)

;; Evil Settings
(setq evil-want-C-u-scroll t)
(require 'evil)
(evil-mode 1)

;; Helm Settings
(require 'helm-config)
;; enable fuzzy matching everywhere
(setq helm-mode-fuzzy-match t)
(setq helm-recentf-fuzzy-match t)
(setq helm-buffers-fuzzy-match t)
(setq helm-locate-fuzzy-match t)
(setq helm-M-x-fuzzy-match t)
(setq helm-semantic-fuzzy-match t)
(setq helm-imenu-fuzzy-match t)
(setq helm-apropos-fuzzy-match t)
(setq helm-lisp-fuzzy-match t)
(setq helm-completion-in-region-fuzzy-match t)
(setq helm-candidate-number-limit 50)
(require 'helm)
(helm-mode 1)

;; Powerline Settings
(require 'powerline)
(powerline-default-theme)
(setq powerline-default-separator 'bar)

;;--------------------
;;---- Appearance ----
;;--------------------
(menu-bar-mode -1)
(tool-bar-mode -1)
(scroll-bar-mode -1)
(add-to-list 'initial-frame-alist '(fullscreen . maximized))

(set-frame-font "Fantasque Sans Mono 10" nil t)

;; Color scheme
(require 'gruvbox-theme)
(load-theme 'gruvbox-dark-hard)

;;----------------------
;;---- Default Vars ----
;;----------------------
;; Stop the blips
(setq visible-bell t)

;; Stop prompt for symlinks
(setq vc-follow-symlinks t)

;; Editor Settings
(setq-default indent-tabs-mode t)
(setq-default tab-width 4)
(add-hook 'prog-mode-hook 'linum-mode)
(add-hook 'prog-mode-hook 'line-number-mode t)
(add-hook 'prog-mode-hook 'column-number-mode t)

;;---------------------
;;---- Keybindings ----
;;---------------------

;; Helm
(global-set-key (kbd "M-x") #'helm-M-x)
(global-set-key (kbd "C-x r b") #'helm-filtered-bookmarks)
(global-set-key (kbd "C-x C-f") #'helm-find-files)
(global-set-key (kbd "C-x b") #'helm-buffers-list)

;; Window movement
(global-set-key (kbd "C-; h") 'windmove-left)
(global-set-key (kbd "C-; j") 'windmove-down)
(global-set-key (kbd "C-; k") 'windmove-up)
(global-set-key (kbd "C-; l") 'windmove-right)

;; Get rid of easily fat fingered keybinds
(global-unset-key (kbd "C-x C-c"))
(global-unset-key (kbd "C-z"))

;; Fuck the mouse (this doesn't work for some reason)
(dolist (k '([mouse-1] [down-mouse-1] [drag-mouse-1] [double-mouse-1] [triple-mouse-1]  
             [mouse-2] [down-mouse-2] [drag-mouse-2] [double-mouse-2] [triple-mouse-2]
             [mouse-3] [down-mouse-3] [drag-mouse-3] [double-mouse-3] [triple-mouse-3]
             [mouse-4] [down-mouse-4] [drag-mouse-4] [double-mouse-4] [triple-mouse-4]
             [mouse-5] [down-mouse-5] [drag-mouse-5] [double-mouse-5] [triple-mouse-5]))
  (global-unset-key k))

;;-----------------
;;---- Aliases ----
;;-----------------
(defalias 'ke 'kill-emacs)
(defalias 'io 'init-open)
(defalias 'il 'init-load)
(defalias 's 'shell)

;;-------------------
;;---- Functions ----
;;-------------------
;; Init file shortcut
(defun init-open ()
  "Open the users init file in the current buffer."
  (interactive)
  (find-file user-init-file))

(defun init-load ()
  "Open the users init file in the current buffer."
  (interactive)
  (load-file user-init-file))

;; Enable all functions
(setq disabled-command-function nil)

;; Move the turds
(setq backup-by-copying t)
(setq temporary-file-directory "~/.emacs.d/backups")
(setq backup-directory-alist
          `((".*" . ,temporary-file-directory)))
    (setq auto-save-file-name-transforms
          `((".*" ,temporary-file-directory t)))
;;(setq backup-directory-alist '(("" . "~/.emacs.d/backups")))
