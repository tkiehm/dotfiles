Dotfiles
========

![Screenshot](screenshot.png)

Installation
------------

This repo conforms to a format that allows for easy symlink managment using [GNU Stow](https://www.gnu.org/software/stow/).

The included install script will install __ALL__ packages using Stow. 

Alternatively, you can select individual packages:

```bash
# 'stow' your chosen packages
$ cd ~/dotfiles
$ stow neovim
$ stow tmux zsh
```

Included Packages
-----------------

   - Window Manager : `i3-gaps`

   - Compositor : `Compton`

   - Bar : `Polybar`

   - Launcher : `Rofi`

   - Terminal Emulator : `Termite`

   - Shell : `Zsh`

   - Terminal Multiplexer : `Tmux`

   - Editors : `Emacs`, `Neovim`

Links
-----

   - [Fantasque Sans](https://github.com/belluzj/fantasque-sans)

   - [Gruvbox](https://github.com/morhetz/gruvbox)

   - [Oh-My-Zsh](https://github.com/robbyrussell/oh-my-zsh)

   - [Vundle](https://github.com/VundleVim/Vundle.vim)

   - [Neovim Remote](https://github.com/mhinz/neovim-remote)

   - [Tmux Plugin Manager](https://github.com/tmux-plugins/tpm)
