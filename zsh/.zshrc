# $PATH Changes
export PATH=/opt/android-studio/bin:$HOME/.cargo/bin:$HOME/.local/bin:$HOME/bin:/usr/local/bin:$PATH
#export RUST_SRC_PATH=/usr/lib/rustlib/src/rust/src

# Path to oh-my-zsh installation.
export ZSH=~/.oh-my-zsh

ZSH_THEME="amuse-clean"

# Case-sensitive completion.
CASE_SENSITIVE="false"

# Hyphen-insensitive completion
HYPHEN_INSENSITIVE="true"

# Bi-weekly auto-update checks.
DISABLE_AUTO_UPDATE="false"

# How often to auto-update (in days).
export UPDATE_ZSH_DAYS=13

# ls colors
 DISABLE_LS_COLORS="false"

# Auto-setting terminal title.
DISABLE_AUTO_TITLE="false"

# Command auto-correction.
ENABLE_CORRECTION="false"

COMPLETION_WAITING_DOTS="false"

# Disable marking untracked files
DISABLE_UNTRACKED_FILES_DIRTY="true"

# Command execution time.
# Three formats: "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
HIST_STAMPS="yyyy-mm-dd"

# Plugins can be found in ~/.oh-my-zsh/plugins/*
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Format: plugins=(rails git textmate ruby lighthouse)
plugins=(git)

source $ZSH/oh-my-zsh.sh

#
#  ---------- ALIASES  ----------
#
DOT_DIR="$HOME/dotfiles"
SCRIPT_DIR=("$DOT_DIR/scripts")
DEV_PATH="$HOME/Dev"

EDITOR="nvim"

# Unix Conveniences
alias lsa="ls -a"
alias cdd="cd $DEV_PATH"

# Colors
alias watch='watch --color'

# Dotfiles (assuming ~/dotfiles location)
alias dotcommit='( cd $DOT_DIR && echo -n "Commit message: " && read message && git commit -a -m "$message" )'
alias dotcd="cd $DOT_DIR"
alias dotpush="( cd $DOT_DIR ; git push )"
alias dotstatus="( cd $DOT_DIR ; git status )"
alias dotpull="( cd $DOT_DIR ; git pull )"

# Config shortcuts
alias zshconfig="$EDITOR $HOME/.zshrc"
alias zshsource='source $HOME/.zshrc'
alias vimconfig="$EDITOR $HOME/.config/nvim/init.vim"
alias tmuxconfig="$EDITOR $HOME/.tmux.conf"
alias tmuxsource="tmux source-file $HOME/.tmux.conf"
alias termconfig="$EDITOR $HOME/.config/termite/config"
alias natorconfig="$EDITOR $HOME/.config/terminator/config"
alias i3config="$EDITOR $HOME/.config/i3/config"
alias pbarconfig="$EDITOR $HOME/.config/polybar/config"

# Package Manager
alias pacs="sudo pacman -S"
alias pacss="pacman -Ss"
alias pacu="sudo pacman -Syu"
alias pacr="sudo pacman -R"
alias pacq="pacman -Q"
alias aurs="pacaur -S"
alias aurss="pacaur -Ss"
alias auru="pacaur -Syu"
alias aurr="pacaur -R"
alias aurq="pacaur -Q"

# Faster tmux manipulation
# Always use tmux session in terminal
#[ -z "$TMUX"  ] && {tmux new-session -A -s base;}
alias tmx="tmux new-session -A -s base"
alias tmxl="tmux list-session"
alias tmxa="tmux attach -t"
alias tmxk="tmux kill-session -t"
alias tmxn="tmux new-session -s"

# Neovim stuff
# Aliases for opening file from :term inside neovim 
# using neovim-remote (https://github.com/mhinz/neovim-remote).
if [ -n "${NVIM_LISTEN_ADDRESS+x}" ]; then
  alias s='nvr -o'
  alias v='nvr -O'
  alias t='nvr --remote-tab'
fi

# Miscellaneous 
alias cols='color_columns'
alias blocks='color_blocks'

#
#  -------- Miscellaneous --------
#

# Have less display colours
# from: https://wiki.archlinux.org/index.php/Color_output_in_console#man
export LESS_TERMCAP_mb=$'\e[1;31m'     # begin bold
export LESS_TERMCAP_md=$'\e[1;92m'     # begin blink
export LESS_TERMCAP_so=$'\e[01;49;31m' # begin reverse video
export LESS_TERMCAP_us=$'\e[01;34m'    # begin underline
export LESS_TERMCAP_me=$'\e[37m'        # reset bold/blink
export LESS_TERMCAP_se=$'\e[37m'        # reset reverse video
export LESS_TERMCAP_ue=$'\e[37m'        # reset underline
export GROFF_NO_SGR=1                  # for konsole and gnome-terminal
