#!/bin/bash
shadow_radius="14"
shadow_opacity="0.83"
shadow_offset_x="-8"
shadow_offset_y="-8"
fade_in_step="0.012"
fade_out_step="1"
fade_delta="2"
shadow_red="0.018"
shadow_blue="0.01"
shadow_green="0.01"

killall compton
eval compton -c -r $shadow_radius -o $shadow_opacity -l $shadow_offset_x -t $shadow_offset_y 	-I $fade_in_step -O $fade_out_step -D $fade_delta --shadow-red $shadow_red 	--shadow-blue $shadow_blue --shadow-green $shadow_green


