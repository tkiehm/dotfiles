"   Plugins {{{1
"-----------------------------------------
set nocompatible              " be iMproved, required
filetype off                  " required
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
Plugin 'VundleVim/Vundle.vim'
" Tpope
Plugin 'tpope/vim-fugitive'
Plugin 'tpope/vim-obsession'
Plugin 'tpope/vim-surround'
Plugin 'scrooloose/nerdcommenter'
"airline
Plugin 'vim-airline/vim-airline'
Plugin 'vim-airline/vim-airline-themes'
" Tools
Plugin 'ctrlpvim/ctrlp.vim'
Plugin 'w0rp/ale'
Plugin 'Shougo/deoplete.nvim'
Plugin 'jiangmiao/auto-pairs'
" Tags
Plugin 'universal-ctags/ctags'
Plugin 'majutsushi/tagbar'
" C Family
Plugin 'Rip-Rip/clang_complete'
Plugin 'octol/vim-cpp-enhanced-highlight'
" Rust 
Plugin 'rust-lang/rust.vim'
Plugin 'sebastianmarkow/deoplete-rust'
" Haskell
Plugin 'neovimhaskell/haskell-vim'
" Web Junk
Plugin 'pangloss/vim-javascript'
Plugin 'leafgarland/typescript-vim'
Plugin 'alvan/vim-closetag'
" Appearance
Plugin 'crusoexia/vim-monokai'
Plugin 'fxn/vim-monochrome'
Plugin 'altercation/vim-colors-solarized'
Plugin 'morhetz/gruvbox'
Plugin 'NLKNguyen/papercolor-theme'
Plugin 'lilydjwg/colorizer'
call vundle#end()            " required
"}}}
"  Options {{{1
"-----------------------------------------
filetype plugin indent on    " required
syntax on
set autochdir number hidden
set nocompatible
set autoindent noexpandtab tabstop=4 shiftwidth=4
set colorcolumn=90
set showcmd 
set cursorline
set lazyredraw
set showmatch incsearch hlsearch
set foldenable 
set foldlevelstart=10
set foldnestmax=10
set foldmethod=indent
let g:tagbar_width=35
set modelines=2
set completeopt-=preview
"}}}
"  Appearance {{{1
"-----------------------------------------
let $NVIM_TUI_ENABLE_TRUE_COLOR=1
set termguicolors
set background=dark
colorscheme gruvbox
let g:gruvbox_contrast_light = 'medium'
let g:gruvbox_contrast_dark = 'hard'
"}}}
"  Keybinds {{{1
"-----------------------------------------
let mapleader = ","

" folds
nnoremap <space> za 

" highlight C comments
nnoremap <leader>ch /\/\/.*\n<cr>
" brackets
inoremap {<CR> {<CR>}<C-o>O

" search
nnoremap <leader><space> :nohlsearch<cr>

" netrw
nnoremap - :e.<cr>

" movement mappings
nnoremap j gj
nnoremap k gk

nnoremap H 0
nnoremap L $
vnoremap H 0
vnoremap L $

nnoremap <C-h> <C-w>h
nnoremap <C-j> <C-w>j
nnoremap <C-k> <C-w>k
nnoremap <C-l> <C-w>l

tnoremap <C-h> <C-\><C-n><C-w>h
tnoremap <C-j> <C-\><C-n><C-w>j
tnoremap <C-k> <C-\><C-n><C-w>k
tnoremap <C-l> <C-\><C-n><C-w>l

" system clipboard
noremap <leader>y "+y
noremap <leader>p "+p

" hex viewing
nnoremap <leader>xx :%!xxd<cr>
nnoremap <leader>xc :%!xxd -r<cr>

" pane management
nnoremap <leader>v :vsp<cr>
nnoremap <leader>s :sp<cr>
nnoremap <Leader>1 :vertical resize -5<cr>
nnoremap <Leader>2 :vertical resize +5<cr>
nnoremap <Leader>3 <C-W>=
nnoremap <Leader>4 :resize -5<cr>
nnoremap <Leader>5 :resize +5<cr>

" quick buffer switch
nnoremap <leader>b :ls<cr>:b 

" file management
nnoremap <leader>q :q<cr>
nnoremap <leader>w :w<cr>
nnoremap <leader>es :source $MYVIMRC<cr>
nnoremap <leader>ev :sp $MYVIMRC<cr>

" ctags
nnoremap <leader>a :TagbarOpen 'j'<CR>

" fugitive (git) management
nnoremap <leader>gs :Gstatus<cr>
nnoremap <leader>gc :Gcommit<cr>
nnoremap <leader>gg :Gpull<cr>
nnoremap <leader>gp :Gpush<cr>
nnoremap <leader>gd :Gdelete<cr>
nnoremap <leader>gb :Git branch<space>
nnoremap <leader>go :Git checkout<space>

" session manipulation
nnoremap <leader>sm :mksession! ~/.vim_session <cr>
nnoremap <leader>so :source ~/.vim_session <cr>
nnoremap <leader>st :Obsess<cr>
nnoremap <leader>sq :Obsess<cr>
nnoremap <leader>ss :source<cr>

" terminals manipulation
tnoremap <leader>q <C-\><C-n>
noremap <leader>te <C-\><C-n>:term<cr>

" tab manipulation
nnoremap <leader>tt :tabe<cr>
nnoremap <leader>tn :tabn<cr>
nnoremap <leader>tb :tabp<cr>
tnoremap <leader>tt <C-\><C-n>:tabe<cr>
tnoremap <leader>tn <C-\><C-n>:tabn<cr>
tnoremap <leader>tb <C-\><C-n>:tabp<cr>
"}}}
"  Plugin Settings {{{1
"-----------------------------------------
" Cpp Syntax settings
"-----------------------------------------
let g:cpp_class_scope_hightlight = 1
let g:cpp_member_variable_highlight = 1
let g:cpp_class_decl_highlight = 1

" Deoplete settings
"-----------------------------------------
let g:deoplete#enable_at_startup = 1

" ALE settings
"-----------------------------------------

" Haskell-vim settings
"-----------------------------------------
let g:haskell_enable_quantification = 1
let g:haskell_enable_recursivedo = 1
let g:haskell_enable_arrowsyntax = 1
let g:haskell_enable_pattern_synonyms = 1
let g:haskell_enable_typeroles = 1
let g:haskell_enable_static_pointers = 1
let g:haskell_backpack = 1

" CtrlP settings
"-----------------------------------------
let g:ctrlp_switch_buffer = 0
let g:ctrlp_working_path_mode = 'ra'

" Airline settings
"-----------------------------------------
let g:airline#extensions#tabline#enable = 1
let g:airline#extensions#tabline#left_sep = ' '
let g:airline#extensions#tabline#left_sep_alt = '|'
let g:airline_powerline_fonts = 1

"}}}
"  Language Settings {{{1
"-----------------------------------------
augroup configgroup
    autocmd!
    autocmd FileType java setlocal noexpandtab
    autocmd FileType java setlocal list
    autocmd FileType java setlocal listchars=tab:+\ ,eol:-
    autocmd FileType java setlocal formatprg=par\ -w80\ -T4
    autocmd FileType ruby setlocal tabstop=2
    autocmd FileType ruby setlocal shiftwidth=2
    autocmd FileType ruby setlocal softtabstop=2
    autocmd FileType ruby setlocal commentstring=#\ %s
    autocmd FileType python setlocal commentstring=#\ %s
    autocmd BufEnter *.cls setlocal filetype=java
    autocmd BufEnter *.zsh-theme setlocal filetype=zsh
    autocmd BufEnter Makefile setlocal noexpandtab
    autocmd BufEnter *.sh setlocal tabstop=2
    autocmd BufEnter *.sh setlocal shiftwidth=2
    autocmd BufEnter *.sh setlocal softtabstop=2
    autocmd BufEnter *.ts setlocal filetype=javascript
    autocmd BufEnter *.ts setlocal shiftwidth=2
augroup END
"}}}
"  Vim Turds  {{{1
"-----------------------------------------
set backup
set backupdir=~/.vim-tmp,~/.tmp,~/tmp,/var/tmp,/tmp
set backupskip=/tmp/*,/private/tmp/*
set directory=~/.vim-tmp,~/.tmp,~/tmp,/var/tmp,/tmp
set writebackup

"}}}
"" vim:foldmethod=marker:foldlevel=0
