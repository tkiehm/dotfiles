rofi_args='-lines 4'

options="
lock\n\
logout\n\
reboot\n\
shutdown\n"

choice=`echo -e $options | rofi -dmenu $rofi_args -font "Fantasque Sans Mono 13"`
echo $choice
case $choice in
	"lock") `i3lock`;;
	"logout") `i3-msg exit`;;
	"reboot") `systemctl reboot`;;
	"shutdown") `systemctl poweroff`;;
esac
