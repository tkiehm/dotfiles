#!/bin/bash

packages=("compton emacs i3 neovim polybar rofi scripts terminator termite tmux zsh")
stow_flags=(" --no-folding -v ")
if [ "$1" = "-D" ]; then
	stow $1 $stow_flags $packages
else
	stow $stow_flags $packages
fi
